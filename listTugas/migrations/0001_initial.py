# Generated by Django 2.1.1 on 2019-09-09 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tugas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matkul', models.CharField(max_length=10)),
                ('keterangan', models.CharField(max_length=30)),
                ('deadline', models.DateTimeField()),
            ],
        ),
    ]
