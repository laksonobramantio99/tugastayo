from django.urls import path
from .views import *

urlpatterns = [
    path('', showTugas, name='showTugas'),
    path('table', showTugas_table, name='showTugas_table')
]

