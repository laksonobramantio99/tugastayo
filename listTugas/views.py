from django.shortcuts import render
from .models import Tugas
from datetime import datetime
from pytz import timezone
import pytz
import json

jakarta_zone = timezone('Asia/Jakarta')

class Deadline():
    def __init__(self):
        self.deadline = ''
        self.timeleft = ''

# Create your views here.
def showTugas(request):
    listTugas = Tugas.objects.all().order_by('deadline')
    
    for i in listTugas:
        waktu = i.deadline
        temp = Deadline()
        waktu = waktu.astimezone(jakarta_zone)
        temp.deadline = waktu.strftime('%A | %b %d, %Y | %H:%M')
        if (waktu < datetime.now(tz=pytz.utc)):
            temp.timeleft = 'EXPIRED!'
        else:
            diff = waktu - datetime.now(tz=pytz.utc)
            temp.timeleft = '{} day(s) {} hour(s) {} minute(s)'.format(diff.days, 
                    diff.seconds//3600, (diff.seconds%3600)//60)
        i.deadline = temp

    response = {
        'listTugas': listTugas,
        'jumlahTugas': len(listTugas)
    }
    return render(request, 'listTugas.html', response)

def showTugas_table(request):
    listTugas = Tugas.objects.all().order_by('deadline')
    
    for i in listTugas:
        waktu = i.deadline
        temp = Deadline()
        waktu = waktu.astimezone(jakarta_zone)
        temp.deadline = waktu.strftime('%Y-%m-%d %H:%M, %A')
        if (waktu < datetime.now(tz=pytz.utc)):
            temp.timeleft = 'EXPIRED!'
        else:
            diff = waktu - datetime.now(tz=pytz.utc)
            temp.timeleft = '{} day(s) {} hour(s) {} minute(s)'.format(diff.days, 
                    diff.seconds//3600, (diff.seconds%3600)//60)
        i.deadline = temp

    response = {
        'listTugas': listTugas,
        'jumlahTugas': len(listTugas)
    }
    return render(request, 'listTugas-table.html', response)
