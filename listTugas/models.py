from django.db import models

# Create your models here.
class Tugas(models.Model):
    matkul = models.CharField(max_length=100)
    keterangan = models.TextField()
    deadline = models.DateTimeField()